#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Usage: $0 <version> <port>"
  exit 1
fi

version=$1
port=$2

docker build -t classifier-frontend-v$version .

docker run -d -p $port:80 classifier-frontend-v$version
