import { Fragment, ReactElement, useState } from "react";

import { Photo } from "../../types/photo";
import { Button, Card, Col, FormControl, Row } from "react-bootstrap";
import { getFullFormatPhotoUrl } from "../../helpers/create-photos-url";
import { useMutation } from "react-query";
import { deletePhoto, uploadPhoto } from "../../services/api-actions";

export type PhotosTableEditableProps = {
  plantId: number;
  photos: Photo[];
  setPhotos: (newPhotos: Photo[]) => void;
};

export function PhotosTableEditable({
  plantId,
  photos,
  setPhotos,
}: PhotosTableEditableProps): ReactElement {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [description, setDescription] = useState<string>("");

  const deletePhotoMutation = useMutation(deletePhoto);
  const uploadPhotoMutation = useMutation(uploadPhoto);

  return (
    <Fragment>
      <h1>Фотографии</h1>
      <Row
        xxl="auto"
        xl="auto"
        lg="auto"
        md="auto"
        sm="auto"
        xs="auto"
        className="plant-card-table"
      >
        {photos.map((photo, index) => (
          <Col key={photo.id}>
            <Card className="photo-card">
              <Card.Header>
                <Button
                  variant="danger"
                  onClick={(e) => {
                    deletePhotoMutation.mutate(photo.id);
                    setPhotos(photos.filter((_, i) => i !== index));
                  }}
                >
                  Удалить
                </Button>
              </Card.Header>
              <Card.Img variant="top" src={getFullFormatPhotoUrl(photo.id)} />
              {photo.description && (
                <Card.Body>
                  <Card.Text>{photo.description}</Card.Text>
                </Card.Body>
              )}
            </Card>
          </Col>
        ))}
        <Col>
          <Card className="photo-card">
            <Card.Header>
              <Button
                disabled={selectedFile === null}
                onClick={(e) => {
                  if (selectedFile !== null) {
                    uploadPhotoMutation.mutate(
                      {
                        info: {
                          plantId: plantId,
                          description: description,
                        },
                        file: selectedFile,
                      },
                      {
                        onSuccess: (response) => {
                          setPhotos(photos.concat(response.data));
                        },
                      },
                    );
                  }
                }}
              >
                Добавить
              </Button>
            </Card.Header>
            <Card.Body>
              <input
                type="file"
                onChange={(e) => {
                  if (e.target.files !== null) {
                    setSelectedFile(e.target.files[0]);
                  }
                }}
              />
              <div>Описание фотографии:</div>
              <FormControl
                as="textarea"
                rows={3}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Fragment>
  );
}
