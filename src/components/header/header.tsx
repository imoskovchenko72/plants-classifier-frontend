import { ReactElement, useState } from "react";
import {
  Container,
  Navbar,
  Form,
  Row,
  Col,
  Button,
  Nav,
  NavDropdown,
} from "react-bootstrap";
import { AppRoute } from "../../app/app-types";
import { createSearchParams, useNavigate } from "react-router-dom";

export function Header(): ReactElement {
  const navigate = useNavigate();
  const [query, setQuery] = useState<string>("");

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href={AppRoute.Root}>Classifier</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href={AppRoute.Root}>Главная</Nav.Link>
          <Nav.Link href={AppRoute.Catalog}>Каталог</Nav.Link>
          <Nav.Link href={AppRoute.Classification}>Определитель</Nav.Link>
          <NavDropdown title="Добавить" id="basic-nav-dropdown">
            <NavDropdown.Item href={AppRoute.CreatePlant}>Вид</NavDropdown.Item>
            <NavDropdown.Item href={AppRoute.CreateTaxon}>
              Таксон высшего порядка
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
        <Form>
          <Row>
            <Col xs="auto">
              <Form.Control
                type="text"
                placeholder="Поиск по названию"
                className=" mr-sm-2"
                onChange={(e) => setQuery(e.target.value)}
              />
            </Col>
            <Col xs="auto">
              <Button
                variant="secondary"
                type="submit"
                onClick={(event) => {
                  event.preventDefault();
                  navigate({
                    pathname: AppRoute.Search,
                    search: createSearchParams({
                      query: query,
                    }).toString(),
                  });
                }}
              >
                Найти
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    </Navbar>
  );
}
