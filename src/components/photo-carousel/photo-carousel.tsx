import React, { ReactElement } from "react";
import { Card, Carousel } from "react-bootstrap";
import { Photo } from "../../types/photo";
import { getFullFormatPhotoUrl } from "../../helpers/create-photos-url";

export type PhotoCarouselProps = {
  plantName: string;
  photos: Photo[];
};

export function PhotoCarousel({
  plantName,
  photos,
}: PhotoCarouselProps): ReactElement {
  return photos.length > 0 ? (
    <Carousel variant="dark" className="carousel-photo">
      {photos.map((photo) => (
        <Carousel.Item key={photo.id}>
          <Card>
            <Card.Img
              variant="top"
              src={getFullFormatPhotoUrl(photo.id)}
              alt={plantName}
            />
            <Card.Body>
              <Card.Text>{photo.description}</Card.Text>
            </Card.Body>
          </Card>
        </Carousel.Item>
      ))}
    </Carousel>
  ) : (
    <img
      src={require("../../default_plant_image.png")}
      alt="default photo"
      className="carousel-photo"
    />
  );
}
