import { Fragment, ReactElement } from "react";
import { Accordion, Alert, Col, Container, Row, Stack } from "react-bootstrap";
import { FullPlantInfo } from "../../types/plant";
import { CoordinateWithId } from "../../types/coordinate";
import { TaxonomyLink } from "../taxonomy-link/taxonomy-link";
import { CharacteristicsList } from "../characteristics-list/characteristics-list";
import { Photo } from "../../types/photo";
import { PhotoCarousel } from "../photo-carousel/photo-carousel";
import { PlantCharacteristicsType } from "../../types/plantCharacteristics";
import { WorldMap } from "../world-map/world-map";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type PlantInfoProps = {
  plantInfo: FullPlantInfo;
  plantPhotos: Photo[];
  coordinates: CoordinateWithId[];
};

export function PlantInfo({
  plantInfo,
  plantPhotos,
  coordinates,
}: PlantInfoProps): ReactElement {
  return (
    <Container className="full-plant-info" fluid>
      <Row>
        <Col sm={4} className="mt-2">
          <PhotoCarousel photos={plantPhotos} plantName={plantInfo.name} />
        </Col>
        <Col sm={8}>
          <Accordion defaultActiveKey={["taxonomy"]} alwaysOpen>
            <Stack gap={1} className="mb-2">
              <h1>{makeFirstLetterUpper(plantInfo.name)}</h1>
              <CharacteristicsList
                characteristicsType={"russian_names"}
                characteristics={plantInfo.russianNames}
              />
              {plantInfo.isEndangered && (
                <Alert variant="danger">
                  Вид находится под угрозой исчезновения
                </Alert>
              )}
              <div>{plantInfo.description || "Описание отсутствует"}</div>
              <CharacteristicsList
                characteristicsType={PlantCharacteristicsType.LIFE_FORM}
                characteristics={plantInfo.lifeForm}
              />
            </Stack>

            <Accordion.Item eventKey="taxonomy">
              <Accordion.Header>Систематика</Accordion.Header>
              <Accordion.Body>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Отдел:
                  <TaxonomyLink taxonInfo={plantInfo.division} />
                </Stack>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Класс:
                  <TaxonomyLink taxonInfo={plantInfo.plantClass} />
                </Stack>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Порядок:
                  <TaxonomyLink taxonInfo={plantInfo.ordo} />
                </Stack>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Семейство:
                  <TaxonomyLink taxonInfo={plantInfo.familia} />
                </Stack>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Род:
                  <TaxonomyLink taxonInfo={plantInfo.genus} />
                </Stack>
                <Stack direction="horizontal" gap={3} className="items-list">
                  Вид:
                  <TaxonomyLink taxonInfo={plantInfo.species} />
                </Stack>
              </Accordion.Body>
            </Accordion.Item>
            {coordinates.length > 0 && (
              <Fragment>
                <h3>Координаты находок:</h3>
                <WorldMap coordinates={coordinates} />
              </Fragment>
            )}
            {(plantInfo.shootsType ||
              plantInfo.leafArrangement ||
              plantInfo.leafPlacement ||
              plantInfo.shootsSurface ||
              plantInfo.appendages) && (
              <Accordion.Item eventKey="shoots">
                <Accordion.Header>Побеги</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.SHOOTS_TYPE}
                    characteristics={plantInfo.shootsType}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_ARRANGEMENT
                    }
                    characteristics={plantInfo.leafArrangement}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_PLACEMENT
                    }
                    characteristics={plantInfo.leafPlacement}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.SHOOTS_SURFACE
                    }
                    characteristics={plantInfo.shootsSurface}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.APPENDAGES}
                    characteristics={plantInfo.appendages}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
            {(plantInfo.leafType ||
              plantInfo.leafBladeShape ||
              plantInfo.leafBladeDivision ||
              plantInfo.leafBladeComplexityLevel ||
              plantInfo.leafBladeAttachment ||
              plantInfo.leafPartsShape ||
              plantInfo.leafPartsDivision ||
              plantInfo.leafApex ||
              plantInfo.leafMargin ||
              plantInfo.leafBase ||
              plantInfo.leafSurface ||
              plantInfo.leafAppendages) && (
              <Accordion.Item eventKey="leaves">
                <Accordion.Header>Листья</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.LEAF_TYPE}
                    characteristics={plantInfo.leafType}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_BLADE_SHAPE
                    }
                    characteristics={plantInfo.leafBladeShape}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_BLADE_DIVISION
                    }
                    characteristics={plantInfo.leafBladeDivision}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_BLADE_COMPLEXITY_LEVEL
                    }
                    characteristics={plantInfo.leafBladeComplexityLevel}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_BLADE_ATTACHMENT
                    }
                    characteristics={plantInfo.leafBladeAttachment}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_PARTS_SHAPE
                    }
                    characteristics={plantInfo.leafPartsShape}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_PARTS_DIVISION
                    }
                    characteristics={plantInfo.leafPartsDivision}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.LEAF_APEX}
                    characteristics={plantInfo.leafApex}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.LEAF_MARGIN}
                    characteristics={plantInfo.leafMargin}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.LEAF_BASE}
                    characteristics={plantInfo.leafBase}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.LEAF_SURFACE}
                    characteristics={plantInfo.leafSurface}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.LEAF_APPENDAGES
                    }
                    characteristics={plantInfo.leafAppendages}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
            {(plantInfo.inflorescense ||
              plantInfo.flowerMainColour ||
              plantInfo.flowerColourTints ||
              plantInfo.flowerSpotsAndStripes ||
              plantInfo.flowerSize ||
              plantInfo.perianth ||
              plantInfo.petalsNumber) && (
              <Accordion.Item eventKey="flowers">
                <Accordion.Header>Цветы</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.INFLORESCENSE}
                    characteristics={plantInfo.inflorescense}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.FLOWER_MAIN_COLOUR
                    }
                    characteristics={plantInfo.flowerMainColour}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.FLOWER_COLOUR_TINTS
                    }
                    characteristics={plantInfo.flowerColourTints}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.FLOWER_SPOTS_AND_STRIPES
                    }
                    characteristics={plantInfo.flowerSpotsAndStripes}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.FLOWER_SIZE}
                    characteristics={plantInfo.flowerSize}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.PERIANTH}
                    characteristics={plantInfo.perianth}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.PETALS_NUMBER}
                    characteristics={plantInfo.petalsNumber}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
            {(plantInfo.fruitsType ||
              plantInfo.fruitColour ||
              plantInfo.fruitAppendages) && (
              <Accordion.Item eventKey="fruits">
                <Accordion.Header>Плоды</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.FRUITS_TYPE}
                    characteristics={plantInfo.fruitsType}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.FRUIT_COLOUR}
                    characteristics={plantInfo.fruitColour}
                  />
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.FRUIT_APPENDAGES
                    }
                    characteristics={plantInfo.fruitAppendages}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
            {(plantInfo.conesConsistency || plantInfo.conesShape) && (
              <Accordion.Item eventKey="cones">
                <Accordion.Header>Шишки</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={
                      PlantCharacteristicsType.CONES_CONSISTENCY
                    }
                    characteristics={plantInfo.conesConsistency}
                  />
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.CONES_SHAPE}
                    characteristics={plantInfo.conesShape}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
            {plantInfo.sporangia && (
              <Accordion.Item eventKey="sporangia">
                <Accordion.Header>Спорангии</Accordion.Header>
                <Accordion.Body>
                  <CharacteristicsList
                    characteristicsType={PlantCharacteristicsType.SPORANGIA}
                    characteristics={plantInfo.sporangia}
                  />
                </Accordion.Body>
              </Accordion.Item>
            )}
          </Accordion>
        </Col>
      </Row>
    </Container>
  );
}
