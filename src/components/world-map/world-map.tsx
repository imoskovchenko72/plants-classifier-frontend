import { ReactElement, useEffect } from "react";
import L from "leaflet";
import "leaflet/dist/leaflet.css";

import { CoordinateWithId } from "../../types/coordinate";

export type WorldMapProps = {
  coordinates: CoordinateWithId[];
};

export function WorldMap({ coordinates }: WorldMapProps): ReactElement {
  useEffect(() => {
    let centerLatitude = 0;
    let centerLongitude = 0;
    if (coordinates.length > 0) {
      for (const coordinate of coordinates) {
        centerLatitude += coordinate.latitude / coordinates.length;
        centerLongitude += coordinate.longitude / coordinates.length;
      }
    }

    const map = L.map("leaflet-map").setView(
      [centerLatitude, centerLongitude],
      5,
    );

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: "© OpenStreetMap contributors",
    }).addTo(map);

    const customIcon = new L.Icon({
      iconUrl: require("../../marker.png"),
      iconSize: [25, 25],
      iconAnchor: [0, 0],
      popupAnchor: [0, 0],
    });

    for (const coordinate of coordinates) {
      const text = `<p>Дата: ${coordinate.date || "-"}</p><p>Описание: ${coordinate.description || "-"}</p>`;
      L.marker([coordinate.latitude, coordinate.longitude], {
        icon: customIcon,
      })
        .bindPopup(text)
        .addTo(map);
    }

    return () => {
      map.remove();
    };
  }, [coordinates]);
  return <div id="leaflet-map"></div>;
}
