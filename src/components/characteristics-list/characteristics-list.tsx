import { Fragment, ReactElement } from "react";
import { Badge, Container, Stack } from "react-bootstrap";
import { PlantCharacteristicsType } from "../../types/plantCharacteristics";
import { characteristicsTypeToRussianName } from "../../helpers/characteristics-type-to-russian-name";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type CharacteristicsListProps = {
  characteristicsType: PlantCharacteristicsType | "russian_names";
  characteristics?: string[];
};

export function CharacteristicsList({
  characteristicsType,
  characteristics,
}: CharacteristicsListProps): ReactElement {
  return characteristics ? (
    <Stack direction="horizontal" gap={3} className="items-list">
      {characteristicsType === "russian_names"
        ? "Русскоязычные названия"
        : characteristicsTypeToRussianName(characteristicsType)}
      :
      <Container className="items-list-container">
        {characteristics.map((characteristic) => (
          <Badge bg="secondary" className="p-2 list-item" key={characteristic}>
            {makeFirstLetterUpper(characteristic)}
          </Badge>
        ))}
      </Container>
    </Stack>
  ) : (
    <Fragment />
  );
}
