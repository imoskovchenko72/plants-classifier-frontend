import { ReactElement } from "react";

import { Card } from "react-bootstrap";
import { FullTaxonInfoWithId } from "../../types/taxon";
import { taxonTypeToRussianName } from "../../helpers/taxon-type-to-russian-name";
import { CharacteristicsList } from "../characteristics-list/characteristics-list";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type TaxonInfoProps = {
  taxonInfo: FullTaxonInfoWithId;
};

export function TaxonInfo({ taxonInfo }: TaxonInfoProps): ReactElement {
  return (
    <Card className="p-2">
      <Card.Title>
        {taxonTypeToRussianName(taxonInfo.type)}:{" "}
        {makeFirstLetterUpper(taxonInfo.name)}
      </Card.Title>
      <Card.Text>{taxonInfo.description || "Описание отсутствует"}</Card.Text>
      <CharacteristicsList
        characteristicsType={"russian_names"}
        characteristics={taxonInfo.russianNames}
      />
    </Card>
  );
}
