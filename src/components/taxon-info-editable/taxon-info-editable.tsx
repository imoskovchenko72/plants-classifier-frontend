import React, { ReactElement } from "react";

import { FullTaxonInfo, TaxonType } from "../../types/taxon";
import { Form } from "react-bootstrap";
import { taxonTypeToRussianName } from "../../helpers/taxon-type-to-russian-name";
import { ItemsListEditable } from "../items-list-editable/items-list-editable";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type TaxonInfoEditableProps = {
  taxonInfo: FullTaxonInfo;
  setTaxonInfo: (taxonInfo: FullTaxonInfo) => void;
  isSetTypeForbidden: boolean;
};

export function TaxonInfoEditable({
  taxonInfo,
  setTaxonInfo,
  isSetTypeForbidden,
}: TaxonInfoEditableProps): ReactElement {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="taxonName">
        <Form.Label>Латинское название</Form.Label>
        <Form.Control
          placeholder="magnoliophyta"
          value={makeFirstLetterUpper(taxonInfo.name)}
          onChange={(e) => setTaxonInfo({ ...taxonInfo, name: e.target.value })}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="taxonType">
        <Form.Label>Тип</Form.Label>
        <Form.Select
          disabled={isSetTypeForbidden}
          value={taxonInfo.type}
          onChange={(e) =>
            setTaxonInfo({
              ...taxonInfo,
              type: TaxonType[e.target.value as keyof typeof TaxonType],
            })
          }
        >
          {Object.keys(TaxonType).map((key) => (
            <option key={key} value={key}>
              {taxonTypeToRussianName(TaxonType[key as keyof typeof TaxonType])}
            </option>
          ))}
        </Form.Select>
      </Form.Group>
      <Form.Group className="mb-3" controlId="taxonDescription">
        <Form.Label>Описание</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          value={taxonInfo.description}
          onChange={(e) =>
            setTaxonInfo({ ...taxonInfo, description: e.target.value })
          }
        />
      </Form.Group>
      <ItemsListEditable
        type={"russian_names"}
        items={taxonInfo.russianNames || []}
        setItems={(newRussianNames: string[]) => {
          setTaxonInfo({
            ...taxonInfo,
            russianNames: newRussianNames,
          });
        }}
      />
    </Form>
  );
}
