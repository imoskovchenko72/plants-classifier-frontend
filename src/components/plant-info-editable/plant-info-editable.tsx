import React, { Fragment, ReactElement } from "react";

import { PlantDraft } from "../../types/plant";
import { PlantCharacteristicsEditable } from "../plant-characteristics-editable/plant-characteristics-editable";
import { Form } from "react-bootstrap";
import { ItemsListEditable } from "../items-list-editable/items-list-editable";
import { ChooseTaxonInput } from "../choose-taxon-input/choose-taxon-input";
import { TaxonType } from "../../types/taxon";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type PlantInfoEditableProps = {
  plantInfo: PlantDraft;
  setPlantInfo: (newPlantInfo: PlantDraft) => void;
};

export function PlantInfoEditable({
  plantInfo,
  setPlantInfo,
}: PlantInfoEditableProps): ReactElement {
  return (
    <Fragment>
      <Form>
        <Form.Group className="mb-3" controlId="taxonName">
          <Form.Label>Латинское название</Form.Label>
          <Form.Control
            placeholder="abies sibirica"
            value={makeFirstLetterUpper(plantInfo.name)}
            onChange={(e) =>
              setPlantInfo({ ...plantInfo, name: e.target.value })
            }
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="taxonDescription">
          <Form.Label>Описание</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={plantInfo.description}
            onChange={(e) =>
              setPlantInfo({ ...plantInfo, description: e.target.value })
            }
          />
        </Form.Group>
        <Form.Check
          className="mb-3"
          type="switch"
          label="Исчезающий вид"
          checked={plantInfo.isEndangered}
          onChange={(e) =>
            setPlantInfo({ ...plantInfo, isEndangered: e.target.checked })
          }
        />
        <ItemsListEditable
          type={"russian_names"}
          items={plantInfo.russianNames || []}
          setItems={(newRussianNames: string[]) => {
            setPlantInfo({
              ...plantInfo,
              russianNames: newRussianNames,
            });
          }}
        />
      </Form>
      <ChooseTaxonInput
        type={TaxonType.DIVISION}
        input={plantInfo.division || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, division: newInput })
        }
      />
      <ChooseTaxonInput
        type={TaxonType.CLASS}
        input={plantInfo.plantClass || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, plantClass: newInput })
        }
      />
      <ChooseTaxonInput
        type={TaxonType.ORDO}
        input={plantInfo.ordo || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, ordo: newInput })
        }
      />
      <ChooseTaxonInput
        type={TaxonType.FAMILIA}
        input={plantInfo.familia || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, familia: newInput })
        }
      />
      <ChooseTaxonInput
        type={TaxonType.GENUS}
        input={plantInfo.genus || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, genus: newInput })
        }
      />
      <ChooseTaxonInput
        type={TaxonType.SPECIES}
        input={plantInfo.species || ""}
        setInput={(newInput: string) =>
          setPlantInfo({ ...plantInfo, species: newInput })
        }
      />
      <PlantCharacteristicsEditable
        plantCharacteristics={plantInfo}
        setPlantCharacteristics={(characteristics) =>
          setPlantInfo({ ...plantInfo, ...characteristics })
        }
      />
    </Fragment>
  );
}
