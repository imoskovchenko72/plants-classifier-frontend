import { ReactElement } from "react";
import { Badge } from "react-bootstrap";
import { ShortTaxonInfo } from "../../types/taxon";
import { Link } from "react-router-dom";
import { AppRoute } from "../../app/app-types";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type TaxonomyLinkProps = {
  taxonInfo?: ShortTaxonInfo;
};

export function TaxonomyLink({ taxonInfo }: TaxonomyLinkProps): ReactElement {
  return taxonInfo ? (
    <Link to={AppRoute.Taxon(`${taxonInfo.id}`)} className="link">
      <Badge bg="secondary" className="p-2 list-item">
        {makeFirstLetterUpper(taxonInfo.name)}
      </Badge>
    </Link>
  ) : (
    <Badge bg="secondary" className="p-2 list-item">
      Неизвестно
    </Badge>
  );
}
