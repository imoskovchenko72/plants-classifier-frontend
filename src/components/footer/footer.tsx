import { ReactElement } from "react";
import { Container, Navbar } from "react-bootstrap";

export function Footer(): ReactElement {
  return (
    <Navbar variant="dark" className="footer bg-secondary">
      <Container>
        <Navbar.Text>
          Текстовые данные взты с{" "}
          <a
            href="https://plantarium.ru"
            target="_blank"
            rel="noopener noreferrer"
          >
            плантариума
          </a>
        </Navbar.Text>
      </Container>
    </Navbar>
  );
}
