import React, { ReactElement } from "react";
import { Accordion } from "react-bootstrap";
import { PlantCharacteristics } from "../../types/plant";
import { CharacteristicsListEditable } from "../characteristics-list-editable/characteristics-list-editable";
import { PlantCharacteristicsType } from "../../types/plantCharacteristics";

export type PlantCharacteristicsEditableProps = {
  plantCharacteristics: PlantCharacteristics;
  setPlantCharacteristics: (
    newPlantCharacteristics: PlantCharacteristics,
  ) => void;
};

export function PlantCharacteristicsEditable({
  plantCharacteristics,
  setPlantCharacteristics,
}: PlantCharacteristicsEditableProps): ReactElement {
  return (
    <Accordion defaultActiveKey={["lifeForm"]} alwaysOpen>
      <Accordion.Item eventKey="lifeForm">
        <Accordion.Header>Жизненная форма</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LIFE_FORM}
            characteristics={plantCharacteristics.lifeForm || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                lifeForm: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="shoots">
        <Accordion.Header>Побеги</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.SHOOTS_TYPE}
            characteristics={plantCharacteristics.shootsType || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                shootsType: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_ARRANGEMENT}
            characteristics={plantCharacteristics.leafArrangement || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafArrangement: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_PLACEMENT}
            characteristics={plantCharacteristics.leafPlacement || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafPlacement: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.SHOOTS_SURFACE}
            characteristics={plantCharacteristics.shootsSurface || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                shootsSurface: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.APPENDAGES}
            characteristics={plantCharacteristics.appendages || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                appendages: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="leaves">
        <Accordion.Header>Листья</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_TYPE}
            characteristics={plantCharacteristics.leafType || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafType: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_BLADE_SHAPE}
            characteristics={plantCharacteristics.leafBladeShape || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafBladeShape: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_BLADE_DIVISION}
            characteristics={plantCharacteristics.leafBladeDivision || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafBladeDivision: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_BLADE_COMPLEXITY_LEVEL}
            characteristics={
              plantCharacteristics.leafBladeComplexityLevel || []
            }
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafBladeComplexityLevel: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_BLADE_ATTACHMENT}
            characteristics={plantCharacteristics.leafBladeAttachment || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafBladeAttachment: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_PARTS_SHAPE}
            characteristics={plantCharacteristics.leafPartsShape || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafPartsShape: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_PARTS_DIVISION}
            characteristics={plantCharacteristics.leafPartsDivision || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafPartsDivision: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_APEX}
            characteristics={plantCharacteristics.leafApex || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafApex: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_MARGIN}
            characteristics={plantCharacteristics.leafMargin || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafMargin: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_BASE}
            characteristics={plantCharacteristics.leafBase || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafBase: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_SURFACE}
            characteristics={plantCharacteristics.leafSurface || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafSurface: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.LEAF_APPENDAGES}
            characteristics={plantCharacteristics.leafAppendages || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                leafAppendages: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="flowers">
        <Accordion.Header>Цветы</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.INFLORESCENSE}
            characteristics={plantCharacteristics.inflorescense || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                inflorescense: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FLOWER_MAIN_COLOUR}
            characteristics={plantCharacteristics.flowerMainColour || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                flowerMainColour: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FLOWER_COLOUR_TINTS}
            characteristics={plantCharacteristics.flowerColourTints || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                flowerColourTints: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FLOWER_SPOTS_AND_STRIPES}
            characteristics={plantCharacteristics.flowerSpotsAndStripes || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                flowerSpotsAndStripes: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FLOWER_SIZE}
            characteristics={plantCharacteristics.flowerSize || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                flowerSize: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.PERIANTH}
            characteristics={plantCharacteristics.perianth || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                perianth: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.PETALS_NUMBER}
            characteristics={plantCharacteristics.petalsNumber || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                petalsNumber: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="fruits">
        <Accordion.Header>Плоды</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FRUITS_TYPE}
            characteristics={plantCharacteristics.fruitsType || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                fruitsType: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FRUIT_COLOUR}
            characteristics={plantCharacteristics.fruitColour || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                fruitColour: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.FRUIT_APPENDAGES}
            characteristics={plantCharacteristics.fruitAppendages || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                fruitAppendages: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="cones">
        <Accordion.Header>Шишки</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.CONES_CONSISTENCY}
            characteristics={plantCharacteristics.conesConsistency || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                conesConsistency: newCharacteristics,
              });
            }}
          />
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.CONES_SHAPE}
            characteristics={plantCharacteristics.conesShape || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                conesShape: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="sporangia">
        <Accordion.Header>Спорангии</Accordion.Header>
        <Accordion.Body>
          <CharacteristicsListEditable
            type={PlantCharacteristicsType.SPORANGIA}
            characteristics={plantCharacteristics.sporangia || []}
            setCharacteristics={(newCharacteristics: string[]) => {
              setPlantCharacteristics({
                ...plantCharacteristics,
                sporangia: newCharacteristics,
              });
            }}
          />
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
