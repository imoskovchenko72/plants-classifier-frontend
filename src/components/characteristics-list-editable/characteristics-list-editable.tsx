import React, { ReactElement } from "react";

import { PlantCharacteristicsType } from "../../types/plantCharacteristics";
import { useQuery } from "react-query";
import { getCharacteristics } from "../../services/api-actions";
import { ItemsListEditable } from "../items-list-editable/items-list-editable";

export type CharacteristicsListEditableProps = {
  type: PlantCharacteristicsType;
  characteristics: string[];
  setCharacteristics: (newCharacteristics: string[]) => void;
};

export function CharacteristicsListEditable({
  type,
  characteristics,
  setCharacteristics,
}: CharacteristicsListEditableProps): ReactElement {
  const { data } = useQuery([`characteristics-${type}`], () =>
    getCharacteristics(type),
  );

  return (
    <ItemsListEditable
      type={type}
      items={characteristics}
      setItems={setCharacteristics}
      possibleItems={data}
    />
  );
}
