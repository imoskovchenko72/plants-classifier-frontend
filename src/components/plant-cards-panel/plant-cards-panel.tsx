import { ReactElement } from "react";

import { Col, Row } from "react-bootstrap";
import { PlantCard } from "../plant-card/plant-card";
import { ShortPlantInfo } from "../../types/plant";
import { getMiniaturePhotoUrl } from "../../helpers/create-photos-url";
import { WithRelevanceResponse } from "../../types/responses";

export type PlantCardsPanelProps = {
  plantCards?: (ShortPlantInfo & WithRelevanceResponse)[];
};

export function PlantCardsPanel({
  plantCards,
}: PlantCardsPanelProps): ReactElement {
  return plantCards && plantCards.length > 0 ? (
    <Row
      xxl="auto"
      xl="auto"
      lg="4"
      md="3"
      sm="2"
      xs="1"
      className="plant-card-table"
    >
      {plantCards.map((plant) => (
        <Col key={plant.id} className="plant-card-col">
          <PlantCard
            id={plant.id}
            name={plant.name}
            isEndangered={plant.isEndangered}
            russianName={plant.russianName}
            photoSrc={
              plant.photoId ? getMiniaturePhotoUrl(plant.photoId) : undefined
            }
            relevanceRatio={plant.relevanceRatio}
          />
        </Col>
      ))}
    </Row>
  ) : (
    <h1>Ничего не найдено</h1>
  );
}
