import React, { ReactElement, useState } from "react";
import { Stack, FormControl, Dropdown } from "react-bootstrap";

import { TaxonType } from "../../types/taxon";
import { taxonTypeToRussianName } from "../../helpers/taxon-type-to-russian-name";
import { useQuery } from "react-query";
import { searchTaxa } from "../../services/api-actions";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type ChooseTaxonInputProps = {
  type: TaxonType;
  input: string;
  setInput: (newInput: string) => void;
};

export function ChooseTaxonInput({
  type,
  input,
  setInput,
}: ChooseTaxonInputProps): ReactElement {
  const { data } = useQuery([`search-${type}-${input}`], () =>
    searchTaxa({ query: input, limit: 10, taxonType: type }),
  );

  const [keepOpen, setKeepOpen] = useState<boolean>(false);
  const [onMenu, setOnMenu] = useState<boolean>(false);

  return (
    <Stack direction="horizontal" gap={3} className="items-list">
      {taxonTypeToRussianName(type)}
      <div className="taxon-form">
        <FormControl
          placeholder="Magnoliophyta"
          value={makeFirstLetterUpper(input)}
          onChange={(e) => {
            setInput(e.target.value);
            setKeepOpen(true);
          }}
          onFocus={(e) => setKeepOpen(true)}
          onBlurCapture={(e) => {
            if (!onMenu) {
              setKeepOpen(false);
            }
          }}
        />
        <Dropdown
          show={keepOpen && data !== undefined && data.length > 0}
          className="custom-dropdown"
        >
          <Dropdown.Toggle
            style={{
              height: "0px",
              width: "0px",
              padding: "0",
              color: "transparent",
              border: "0",
              cursor: "default",
            }}
          ></Dropdown.Toggle>
          <Dropdown.Menu
            onMouseEnter={(e) => setOnMenu(true)}
            onMouseLeave={(e) => setOnMenu(false)}
          >
            {data !== undefined &&
              data!.map((taxon) => (
                <Dropdown.Item
                  key={taxon.id}
                  onClick={(e) => {
                    setInput("");
                    setInput(taxon.name);
                    setKeepOpen(false);
                  }}
                >
                  {makeFirstLetterUpper(taxon.name)}
                </Dropdown.Item>
              ))}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </Stack>
  );
}
