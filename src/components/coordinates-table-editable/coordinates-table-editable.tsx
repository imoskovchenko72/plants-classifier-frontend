import { Fragment, ReactElement, useState } from "react";
import { Button, FormControl, Table } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { CoordinateWithId } from "../../types/coordinate";
import { useMutation } from "react-query";
import { deleteCoordinate, createCoordinate } from "../../services/api-actions";
import { getDateString } from "../../helpers/date-convertes";

export type CoordinatesTableEditableProps = {
  plantId: number;
  coordinates: CoordinateWithId[];
  setCoordinates: (newCoordinates: CoordinateWithId[]) => void;
};

export function CoordinatesTableEditable({
  plantId,
  coordinates,
  setCoordinates,
}: CoordinatesTableEditableProps): ReactElement {
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const [latitude, setLatitude] = useState<number>(0);
  const [longitude, setLongitude] = useState<number>(0);
  const [description, setDescription] = useState<string>("");

  const deleteCoordinateMutation = useMutation(deleteCoordinate);
  const createCoordinateMutation = useMutation(createCoordinate);

  return (
    <Fragment>
      <h1>Таблица координат</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Широта</th>
            <th>Долгота</th>
            <th>Описание</th>
            <th>Год</th>
            <th>Месяц</th>
            <th>День</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {coordinates.map((coordinate, index) => (
            <tr key={coordinate.id}>
              <td>{coordinate.latitude}</td>
              <td>{coordinate.longitude}</td>
              <td>{coordinate.description}</td>
              <td>{coordinate.date?.split("-")[0]}</td>
              <td>{coordinate.date?.split("-")[1]}</td>
              <td>{coordinate.date?.split("-")[2]}</td>
              <td>
                <Button
                  variant="danger"
                  onClick={(e) => {
                    deleteCoordinateMutation.mutate(coordinate.id);
                    setCoordinates(coordinates.filter((_, i) => i !== index));
                  }}
                >
                  Удалить
                </Button>
              </td>
            </tr>
          ))}
          <tr>
            <td>
              <input
                className="table-row-input"
                value={latitude}
                type="number"
                onChange={(e) => setLatitude(Number(e.target.value))}
              />
            </td>
            <td>
              <input
                className="table-row-input"
                value={longitude}
                type="number"
                onChange={(e) => setLongitude(Number(e.target.value))}
              />
            </td>
            <td>
              <FormControl
                as="textarea"
                rows={3}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </td>
            <td colSpan={3}>
              <DatePicker
                className="table-row-input"
                selected={selectedDate}
                onChange={(date) => setSelectedDate(date)}
                dateFormat="yyyy-MM-dd"
              />
            </td>
            <td>
              <Button
                onClick={(e) => {
                  createCoordinateMutation.mutate(
                    {
                      latitude: latitude,
                      longitude: longitude,
                      plantId: plantId,
                      date: getDateString(selectedDate),
                      description: description,
                    },
                    {
                      onSuccess: (response) => {
                        setCoordinates(coordinates.concat(response.data));
                      },
                    },
                  );
                }}
              >
                Добавить
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </Fragment>
  );
}
