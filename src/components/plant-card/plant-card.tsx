import { ReactElement } from "react";
import { Alert, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { AppRoute } from "../../app/app-types";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type PlantCardProps = {
  id: number;
  name: string;
  isEndangered: boolean;
  russianName?: string;
  photoSrc?: string;
  relevanceRatio?: number;
};

export function PlantCard({
  id,
  name,
  isEndangered,
  russianName,
  photoSrc,
  relevanceRatio,
}: PlantCardProps): ReactElement {
  return (
    <Link to={AppRoute.Plant(`${id}`)} className="link">
      <Card className="plant-card">
        <Card.Img
          variant="top"
          src={photoSrc || require("../../default_plant_image.png")}
          height={220}
        />
        <Card.Body>
          <Card.Title className="plant-title">
            {makeFirstLetterUpper(name)}
          </Card.Title>
          {isEndangered && (
            <Alert variant="danger">вид под угрозой исчезновения</Alert>
          )}
          {relevanceRatio !== undefined && (
            <Card.Text>Процент совпдания: {relevanceRatio * 100}</Card.Text>
          )}
          {russianName && (
            <Card.Text>{makeFirstLetterUpper(russianName)}</Card.Text>
          )}
        </Card.Body>
      </Card>
    </Link>
  );
}
