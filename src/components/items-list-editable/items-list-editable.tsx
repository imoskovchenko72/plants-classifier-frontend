import React, { ReactElement, useState } from "react";
import {
  Badge,
  Stack,
  Dropdown,
  FormControl,
  CloseButton,
} from "react-bootstrap";
import { PlantCharacteristicsType } from "../../types/plantCharacteristics";
import { characteristicsTypeToRussianName } from "../../helpers/characteristics-type-to-russian-name";
import { getItemsSuggestions } from "../../helpers/get-items-suggestions";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type ItemsListEditableProps = {
  type: PlantCharacteristicsType | "russian_names";
  items: string[];
  setItems: (newCharacteristics: string[]) => void;
  possibleItems?: string[];
};

export function ItemsListEditable({
  type,
  items,
  setItems,
  possibleItems,
}: ItemsListEditableProps): ReactElement {
  const [query, setQuery] = useState<string>("");
  const [keepOpen, setKeepOpen] = useState<boolean>(false);
  const [onMenu, setOnMenu] = useState<boolean>(false);

  return (
    <Stack direction="horizontal" gap={3} className="items-list">
      {type === "russian_names"
        ? "Русскоязычные названия"
        : characteristicsTypeToRussianName(type)}
      <div className="items-list-container">
        {items.map((item, index) => (
          <Badge bg="secondary" className="p-2 list-item" key={item}>
            {makeFirstLetterUpper(item)}
            <CloseButton
              variant="white"
              onClick={(e) => setItems(items.filter((_, i) => i !== index))}
            />
          </Badge>
        ))}
      </div>

      <div style={{ display: "flex", position: "relative" }}>
        <FormControl
          placeholder={type === "russian_names" ? "Название" : "Признак"}
          value={query}
          onChange={(e) => {
            setQuery(e.target.value);
            setKeepOpen(true);
          }}
          onFocus={(e) => setKeepOpen(true)}
          onBlurCapture={(e) => {
            if (!onMenu) {
              setKeepOpen(false);
            }
          }}
        />
        <Dropdown
          className="custom-dropdown"
          show={
            keepOpen &&
            (query.length > 0 ||
              (possibleItems !== undefined && possibleItems.length > 0))
          }
        >
          <Dropdown.Toggle
            style={{
              height: "0px",
              width: "0px",
              padding: "0",
              color: "transparent",
              border: "0",
              cursor: "default",
            }}
          ></Dropdown.Toggle>
          <Dropdown.Menu
            onMouseEnter={(e) => setOnMenu(true)}
            onMouseLeave={(e) => setOnMenu(false)}
          >
            {getItemsSuggestions(items, query, possibleItems).map((value) => (
              <Dropdown.Item
                key={value}
                onClick={(e) => {
                  setQuery("");
                  setItems(items.concat(value));
                  setKeepOpen(false);
                }}
              >
                {value}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </Stack>
  );
}
