import { Fragment, ReactElement } from "react";

import { Form, Stack } from "react-bootstrap";
import { FullTaxonInfoWithId } from "../../types/taxon";
import { CharacteristicsList } from "../characteristics-list/characteristics-list";
import { taxonTypeToRussianName } from "../../helpers/taxon-type-to-russian-name";
import { Link } from "react-router-dom";
import { AppRoute } from "../../app/app-types";
import { WithRelevanceResponse } from "../../types/responses";
import { makeFirstLetterUpper } from "../../helpers/names-converters";

export type TaxonInfoProps = {
  taxonCards?: (FullTaxonInfoWithId & WithRelevanceResponse)[];
};

export function TaxonCardsPanel({ taxonCards }: TaxonInfoProps): ReactElement {
  return taxonCards && taxonCards.length > 0 ? (
    <Fragment>
      {taxonCards.map((taxon) => (
        <Link to={AppRoute.Taxon(`${taxon.id}`)} className="link">
          <Stack gap={3} direction="horizontal" className="taxon-card">
            <div>
              <Form.Text className="taxon-form-text">
                {taxonTypeToRussianName(taxon.type)}:&nbsp;
              </Form.Text>
              {makeFirstLetterUpper(taxon.name)}
            </div>
            {taxon.relevanceRatio !== undefined && (
              <div>
                <Form.Text className="taxon-form-text">
                  Процент совпадения:&nbsp;
                </Form.Text>
                {taxon.relevanceRatio * 100}%
              </div>
            )}
            <div>{taxon.description || "Описание отсутствует"}</div>
            <CharacteristicsList
              characteristicsType={"russian_names"}
              characteristics={taxon.russianNames}
            />
          </Stack>
        </Link>
      ))}
    </Fragment>
  ) : (
    <h1>Ничего не найдено</h1>
  );
}
