import { api } from "./api";
import { ApiRoute } from "./api-constants";
import {
  FullPlantInfo,
  PlantCharacteristics,
  PlantDraft,
  ShortPlantInfo,
} from "../types/plant";
import { FullTaxonInfo, FullTaxonInfoWithId, TaxonType } from "../types/taxon";
import {
  WithLimitRequest,
  WithQueryRequest,
  WithTaxonomyRequest,
} from "../types/requests";
import { PlantCharacteristicsType } from "../types/plantCharacteristics";
import { Photo, ShortPhotoInfo } from "../types/photo";
import { CoordinateInfo, CoordinateWithId } from "../types/coordinate";
import { WithRelevanceResponse } from "../types/responses";

export const getFullPlantInfo = async (id: number) => {
  return await api.get<FullPlantInfo>(ApiRoute.GetFullPlantInfo(id));
};

export const getPlantPhotos = async (id: number) => {
  let result = await api.get<{ photos: Photo[] }>(ApiRoute.GetPlantPhotos(id));
  return result.data!.photos;
};

export const getPlantCoordinates = async (id: number) => {
  let result = await api.get<{ coordinates: CoordinateWithId[] }>(
    ApiRoute.GetPlantCoordinates(id),
  );
  return result.data!.coordinates;
};

export const getFullTaxonInfo = async (id: number) => {
  return await api.get<FullTaxonInfoWithId>(ApiRoute.GetFullTaxonInfo(id));
};

export const getPlantsByTaxon = async (
  request: WithLimitRequest & WithTaxonomyRequest,
) => {
  let result = await api.get<{ plants: ShortPlantInfo[] }>(
    ApiRoute.GetPlantsByTaxon,
    {
      params: {
        taxonId: request.taxonId,
        limit: request.limit,
        offset: request.offset,
      },
    },
  );
  return result.data!.plants;
};

export const searchPlants = async (
  request: WithLimitRequest & WithTaxonomyRequest & WithQueryRequest,
) => {
  let result = await api.get<{ plants: ShortPlantInfo[] }>(
    ApiRoute.SearchPlant,
    {
      params: request,
    },
  );
  return result.data!.plants;
};

export const searchTaxa = async (
  request: WithLimitRequest & WithQueryRequest & { taxonType?: TaxonType },
) => {
  let result = await api.get<{ taxa: FullTaxonInfoWithId[] }>(
    ApiRoute.SearchTaxon,
    {
      params: request,
    },
  );
  return result.data!.taxa;
};

export const getCharacteristics = async (type: PlantCharacteristicsType) => {
  let result = await api.get<{ characteristics: string[] }>(
    ApiRoute.GetCharacteristics,
    { params: { type: type } },
  );
  return result.data!.characteristics;
};

export const getMostRelevantPlants = async (request: {
  request: WithLimitRequest & WithTaxonomyRequest;
  characteristics: PlantCharacteristics & {
    latitude?: number;
    longitude?: number;
  };
}) => {
  let result = await api.patch<{
    plants: (ShortPlantInfo & WithRelevanceResponse)[];
  }>(ApiRoute.ClassifyPlant, request.characteristics, {
    params: request.request,
  });
  return result.data!.plants;
};

export const getMostRelevantTaxa = async (request: {
  request: WithLimitRequest;
  characteristics: PlantCharacteristics & {
    latitude?: number;
    longitude?: number;
  };
}) => {
  let result = await api.patch<{
    taxa: (FullTaxonInfoWithId & WithRelevanceResponse)[];
  }>(ApiRoute.ClassifyTaxon, request.characteristics, {
    params: request.request,
  });
  return result.data!.taxa;
};

export const createTaxon = async (taxonInfo: FullTaxonInfo) => {
  return await api.post<FullTaxonInfoWithId>(ApiRoute.CreateTaxon, taxonInfo);
};

export const createPlant = async (plantInfo: PlantDraft) => {
  return await api.post<FullPlantInfo>(ApiRoute.CreatePlant, plantInfo);
};

export const enrichPlantData = async (plantInfo: PlantDraft) => {
  return await api.patch<PlantDraft>(ApiRoute.EnrichPlantData, plantInfo);
};

export const updateTaxon = async (request: {
  id: number;
  taxonInfo: FullTaxonInfo;
}) => {
  return await api.put<FullTaxonInfoWithId>(
    ApiRoute.UpdateTaxon(request.id),
    request.taxonInfo,
  );
};

export const updatePlant = async (request: {
  id: number;
  plantInfo: PlantDraft;
}) => {
  return await api.put<FullPlantInfo>(
    ApiRoute.UpdatePlant(request.id),
    request.plantInfo,
  );
};

export const deleteTaxon = async (id: number) => {
  return await api.delete<null>(ApiRoute.DeleteTaxon(id));
};

export const deletePlant = async (id: number) => {
  return await api.delete<null>(ApiRoute.DeletePlant(id));
};

export const createCoordinate = async (request: CoordinateInfo) => {
  return await api.post<CoordinateWithId>(ApiRoute.CreateCoordinate, request);
};

export const deleteCoordinate = async (id: number) => {
  return await api.delete<null>(ApiRoute.DeleteCoordinate(id));
};

export const uploadPhoto = async (request: {
  info: ShortPhotoInfo;
  file: File;
}) => {
  const formData = new FormData();
  formData.append("file", request.file);
  formData.append("plantId", `${request.info.plantId}`);
  if (request.info.description !== undefined) {
    formData.append("description", request.info.description);
  }
  return await api.post<Photo>(ApiRoute.UploadPhoto, formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

export const deletePhoto = async (photoId: string) => {
  return await api.delete<null>(ApiRoute.DeletePhoto(photoId));
};
