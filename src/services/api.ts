import axios from "axios";
import applyCaseMiddleware from "axios-case-converter";

import { BACKEND_URL, FRONTEND_URL, REQUEST_TIMEOUT } from "./api-constants";

export const api = applyCaseMiddleware(
  axios.create({
    baseURL: BACKEND_URL,
    timeout: REQUEST_TIMEOUT,
    headers: {
      "Access-Control-Allow-Origin": FRONTEND_URL,
      "Access-Control-Allow-Credentials": true,
    },
    withCredentials: true,
  }),
);

api.defaults.headers.common["Content-Type"] = "application/json";

api.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    return Promise.reject(error);
  },
);
