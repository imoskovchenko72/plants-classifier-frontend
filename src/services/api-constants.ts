export const FRONTEND_URL = process.env.REACT_APP_FRONTEND_URL;
export const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;
export const REQUEST_TIMEOUT = Number(
  process.env.REACT_APP_BACKEND_REQUEST_TIMEOUT,
);

export const ApiRoute = {
  GetFullPlantInfo: (plantId: number) => `/plants/${plantId}/get`,
  GetPlantCoordinates: (plantId: number) => `/plants/${plantId}/coordinates`,
  GetPlantPhotos: (plantId: number) => `/plants/${plantId}/photos`,
  GetFullTaxonInfo: (taxonId: number) => `/taxa/${taxonId}/get`,
  GetFullFormatPhoto: (photoId: string) => `/photos/download/${photoId}`,
  GetMiniaturePhoto: (photoId: string) => `/photos/download/${photoId}/mini`,
  GetPlantsByTaxon: "/plants/get",
  SearchPlant: "/search/plant",
  SearchTaxon: "/search/taxon",
  GetCharacteristics: "/characteristics/get",
  ClassifyPlant: "/classify/plant",
  ClassifyTaxon: "/classify/taxon",
  CreatePlant: "/plants/create",
  CreateTaxon: "/taxa/create",
  EnrichPlantData: "/plants/enrich",
  UpdatePlant: (plantId: number) => `/plants/${plantId}/update`,
  UpdateTaxon: (taxonId: number) => `/taxa/${taxonId}/update`,
  DeletePlant: (plantId: number) => `/plants/${plantId}/delete`,
  DeleteTaxon: (taxonId: number) => `/taxa/${taxonId}/delete`,
  CreateCoordinate: "/coordinates/create",
  DeleteCoordinate: (coordinateId: number) =>
    `/coordinates/${coordinateId}/delete`,
  UploadPhoto: "/photos/upload",
  DeletePhoto: (photoId: string) => `/photos/delete/${photoId}`,
};
