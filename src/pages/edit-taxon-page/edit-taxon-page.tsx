import { Fragment, ReactElement, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { useMutation, useQuery } from "react-query";
import {
  deleteTaxon,
  getFullTaxonInfo,
  updateTaxon,
} from "../../services/api-actions";
import { FullTaxonInfo, TaxonType } from "../../types/taxon";
import { Alert, Button, Container, Spinner, Stack } from "react-bootstrap";
import { AppRoute } from "../../app/app-types";
import { TaxonInfoEditable } from "../../components/taxon-info-editable/taxon-info-editable";

export function EditTaxonPage(): ReactElement {
  const { id } = useParams();
  const parsedId = parseInt(id || "-1", 10) || -1;

  const [taxonInfo, setTaxonInfo] = useState<FullTaxonInfo>({
    name: "",
    description: "",
    type: TaxonType.CLASS,
  });

  const {
    isLoading: isTaxonDataLoading,
    error: taxonError,
    isError: isDataLoadingError,
  } = useQuery(
    [`taxonInfoForEdit-${parsedId}`],
    () => getFullTaxonInfo(parsedId),
    {
      onSuccess: (data) => {
        setTaxonInfo(data.data);
      },
    },
  );

  const updateTaxonMutation = useMutation(updateTaxon);
  const deleteTaxonMutation = useMutation(deleteTaxon);

  const navigate = useNavigate();

  if (deleteTaxonMutation.isSuccess) {
    navigate(AppRoute.Root);
  }
  if (updateTaxonMutation.isSuccess) {
    navigate(AppRoute.Taxon(`${updateTaxonMutation.data.data.id}`));
  }

  return (
    <Fragment>
      <Header />
      {isTaxonDataLoading ||
      updateTaxonMutation.isLoading ||
      deleteTaxonMutation.isLoading ? (
        <Spinner />
      ) : isDataLoadingError ||
        updateTaxonMutation.isError ||
        deleteTaxonMutation.isError ? (
        <Alert variant="danger">Что-то пошло не так </Alert>
      ) : (
        <Container>
          <TaxonInfoEditable
            taxonInfo={taxonInfo}
            setTaxonInfo={setTaxonInfo}
            isSetTypeForbidden
          />
          <Stack gap={3} direction="horizontal" className="mt-3">
            <Button
              variant="secondary"
              onClick={(e) =>
                updateTaxonMutation.mutate({
                  id: parsedId,
                  taxonInfo: taxonInfo,
                })
              }
            >
              Сохранить
            </Button>
            <Button
              variant="danger"
              onClick={(e) => deleteTaxonMutation.mutate(parsedId)}
            >
              Удалить
            </Button>
          </Stack>
        </Container>
      )}
      <Footer />
    </Fragment>
  );
}
