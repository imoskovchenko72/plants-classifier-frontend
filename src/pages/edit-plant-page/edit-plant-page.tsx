import { Fragment, ReactElement, useState } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Row,
  Spinner,
  Stack,
} from "react-bootstrap";
import { useMutation, useQuery } from "react-query";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { useNavigate, useParams } from "react-router-dom";
import { PlantDraft } from "../../types/plant";
import {
  deletePlant,
  getFullPlantInfo,
  getPlantCoordinates,
  getPlantPhotos,
  updatePlant,
} from "../../services/api-actions";
import { AppRoute } from "../../app/app-types";
import { convertFullPlantInfoToPlantDraft } from "../../helpers/plant-converters";
import { PlantInfoEditable } from "../../components/plant-info-editable/plant-info-editable";
import { CoordinatesTableEditable } from "../../components/coordinates-table-editable/coordinates-table-editable";
import { CoordinateWithId } from "../../types/coordinate";
import { Photo } from "../../types/photo";
import { PhotosTableEditable } from "../../components/photos-table-editable/photos-table-editable";

export function EditPlantPage(): ReactElement {
  const { id } = useParams();
  const parsedId = parseInt(id || "-1", 10) || -1;

  const [plantInfo, setPlantInfo] = useState<PlantDraft>({
    name: "",
    description: "",
    isEndangered: false,
  });
  const {
    isLoading: isPlantDataLoading,
    error: PlantError,
    isError: isDataLoadingError,
  } = useQuery(
    [`plantInfoForEdit-${parsedId}`],
    () => getFullPlantInfo(parsedId),
    {
      onSuccess: (data) => {
        setPlantInfo(convertFullPlantInfoToPlantDraft(data.data));
      },
    },
  );

  const [coordinates, setCoordinates] = useState<CoordinateWithId[]>([]);
  const {
    isLoading: isCoordinatesLoading,
    error: coordinatesError,
    isError: isCoordinatesDataLoadingError,
  } = useQuery(
    [`plantCoordinatesForUpdate-${parsedId}`],
    () => getPlantCoordinates(parsedId),
    {
      onSuccess: (data) => {
        setCoordinates(data);
      },
    },
  );

  const [photos, setPhotos] = useState<Photo[]>([]);
  const {
    isLoading: isPhotosLoading,
    error: photosError,
    isError: isPhotosLoadingError,
  } = useQuery(
    [`plantPhotosForUpdate-${parsedId}`],
    () => getPlantPhotos(parsedId),
    {
      onSuccess: (data) => {
        setPhotos(data);
      },
    },
  );

  const updatePlantMutation = useMutation(updatePlant);
  const deletePlantMutation = useMutation(deletePlant);

  const navigate = useNavigate();

  if (deletePlantMutation.isSuccess) {
    navigate(AppRoute.Root);
  }
  if (updatePlantMutation.isSuccess) {
    navigate(AppRoute.Plant(`${updatePlantMutation.data.data.id}`));
  }

  return (
    <Fragment>
      <Header />
      <Container>
        <Row>
          {isPhotosLoading ? (
            <Spinner />
          ) : isPhotosLoadingError ? (
            <Alert>Что-то пошло не так</Alert>
          ) : (
            <Col sm={4}>
              <PhotosTableEditable
                plantId={parsedId}
                photos={photos}
                setPhotos={setPhotos}
              />
            </Col>
          )}
          <Col sm={8}>
            {isPlantDataLoading ||
            updatePlantMutation.isLoading ||
            deletePlantMutation.isLoading ? (
              <Spinner />
            ) : isDataLoadingError ||
              updatePlantMutation.isError ||
              deletePlantMutation.isError ? (
              <Alert variant="danger">Что-то пошло не так </Alert>
            ) : (
              <Fragment>
                <PlantInfoEditable
                  plantInfo={plantInfo}
                  setPlantInfo={setPlantInfo}
                />
                <Stack direction="horizontal" gap={3} className="mt-2">
                  <Button
                    variant="secondary"
                    onClick={(e) =>
                      updatePlantMutation.mutate({
                        id: parsedId,
                        plantInfo: plantInfo,
                      })
                    }
                  >
                    Сохранить
                  </Button>
                  <Button
                    variant="danger"
                    onClick={(e) => deletePlantMutation.mutate(parsedId)}
                  >
                    Удалить
                  </Button>
                </Stack>
              </Fragment>
            )}
            {isCoordinatesLoading ? (
              <Spinner />
            ) : isCoordinatesDataLoadingError ? (
              <Alert variant="danger">Что-то пошло не так </Alert>
            ) : (
              <CoordinatesTableEditable
                coordinates={coordinates}
                setCoordinates={setCoordinates}
                plantId={parsedId}
              />
            )}
          </Col>
        </Row>
      </Container>

      <Footer />
    </Fragment>
  );
}
