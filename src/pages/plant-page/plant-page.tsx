import { Fragment, ReactElement } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useQuery } from "react-query";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import {
  getFullPlantInfo,
  getPlantCoordinates,
  getPlantPhotos,
} from "../../services/api-actions";
import { PlantInfo } from "../../components/plant-info/plant-info";
import { Alert, Button, Container, Spinner } from "react-bootstrap";
import { AppRoute } from "../../app/app-types";

export function PlantPage(): ReactElement {
  const { id } = useParams();
  const parsedId = parseInt(id || "-1", 10) || -1;

  const navigate = useNavigate();

  const {
    isLoading: isPlantDataLoading,
    data: plantData,
    error: plantError,
    isError: isPlantDataLoadingError,
  } = useQuery([`plantInfo-${parsedId}`], () => getFullPlantInfo(parsedId));

  const {
    isLoading: isPhotosLoading,
    data: photosData,
    error: photosError,
    isError: isPhotosDataLoadingError,
  } = useQuery([`plantPhotos-${parsedId}`], () => getPlantPhotos(parsedId));

  const {
    isLoading: isCoordinatesLoading,
    data: coordinatesData,
    error: coordinatesError,
    isError: isCoordinatesDataLoadingError,
  } = useQuery([`plantCoordinates-${parsedId}`], () =>
    getPlantCoordinates(parsedId),
  );

  return (
    <Fragment>
      <Header />
      {isPlantDataLoading || isPhotosLoading || isCoordinatesLoading ? (
        <Container
          style={{
            padding: "20% 50%",
          }}
        >
          <Spinner />
        </Container>
      ) : isPlantDataLoadingError ||
        isPhotosDataLoadingError ||
        isCoordinatesDataLoadingError ? (
        <Alert variant="danger">Что-то пошло не так</Alert>
      ) : (
        <Container>
          <PlantInfo
            plantInfo={plantData!.data}
            plantPhotos={photosData!}
            coordinates={coordinatesData!}
          />
          <Button
            className="my-3"
            variant="secondary"
            onClick={(event) => {
              event.preventDefault();
              navigate(AppRoute.EditPlant(id));
            }}
          >
            Редактировать
          </Button>
        </Container>
      )}
      <Footer />
    </Fragment>
  );
}
