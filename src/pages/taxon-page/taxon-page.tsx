import { Fragment, ReactElement, useState } from "react";
import { createSearchParams, useNavigate, useParams } from "react-router-dom";
import { useQuery } from "react-query";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { getFullTaxonInfo, getPlantsByTaxon } from "../../services/api-actions";
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from "react-bootstrap";
import { TaxonInfo } from "../../components/taxon-info/taxon-info";
import { PlantCardsPanel } from "../../components/plant-cards-panel/plant-cards-panel";
import { AppRoute } from "../../app/app-types";

export function TaxonPage(): ReactElement {
  const { id } = useParams();
  const parsedId = parseInt(id || "-1", 10) || -1;

  const {
    isLoading: isTaxonDataLoading,
    data: taxonData,
    error: taxonError,
    isError,
  } = useQuery([`taxonInfo-${parsedId}`], () => getFullTaxonInfo(parsedId));

  const navigate = useNavigate();
  const [query, setQuery] = useState<string>("");

  const { isLoading: isCardsLoading, data: cardsData } = useQuery(
    [`cardsByTaxon-${parsedId}`],
    () => getPlantsByTaxon({ taxonId: parsedId }),
  );
  return (
    <Fragment>
      <Header />
      <Container>
        {isTaxonDataLoading ? (
          <Spinner />
        ) : isError ? (
          <Alert variant="danger">Что-то пошло не так </Alert>
        ) : (
          <Container className="mt-3">
            <TaxonInfo taxonInfo={taxonData!.data} />
            <div className="control-buttons">
              <Button
                variant="secondary"
                onClick={(event) => {
                  event.preventDefault();
                  navigate(AppRoute.EditTaxon(id));
                }}
              >
                Редактировать
              </Button>
            </div>
          </Container>
        )}
        <Row>
          <Col xs="auto">
            <Form.Control
              type="text"
              placeholder="Поиск по субтаксонам"
              className=" mr-sm-2"
              onChange={(e) => setQuery(e.target.value)}
            />
          </Col>
          <Col xs="auto">
            <Button
              variant="secondary"
              type="submit"
              onClick={(event) => {
                event.preventDefault();
                navigate({
                  pathname: AppRoute.Search,
                  search: createSearchParams({
                    query: query,
                    taxonId: `${parsedId}`,
                  }).toString(),
                });
              }}
            >
              Найти
            </Button>
          </Col>
        </Row>
        <div className="control-buttons">
          <Button
            variant="secondary"
            onClick={(event) => {
              event.preventDefault();
              navigate({
                pathname: AppRoute.Classification,
                search: createSearchParams({
                  taxonId: `${parsedId}`,
                }).toString(),
              });
            }}
          >
            Определитель внутри таксона
          </Button>
        </div>

        <h1>Субтаксоны:</h1>
        {isCardsLoading ? (
          <Spinner />
        ) : (
          <PlantCardsPanel plantCards={cardsData} />
        )}
      </Container>
      <Footer />
    </Fragment>
  );
}
