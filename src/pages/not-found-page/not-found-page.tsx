import { Fragment, ReactElement } from "react";
import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";

export function NotFoundPage(): ReactElement {
  return (
    <Fragment>
      <Header />
      <h1>Страница не найдена</h1>
      <Footer />
    </Fragment>
  );
}
