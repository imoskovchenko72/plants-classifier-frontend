import { Fragment, ReactElement } from "react";
import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { Card } from "react-bootstrap";

export function MainPage(): ReactElement {
  return (
    <Fragment>
      <Header />
      <Card>
        <Card.Header>
          Атлас и определитель растений Западной Сибири и Урала
        </Card.Header>
        <Card.Body>
          Данный ресурс является базой знаний о растениях Западной сибири,
          собранной в ходе экспедиций и дополненной из различных открытых
          источников.
        </Card.Body>
        <Card.Body>
          Основная задача ресурса - поддержание данных о растениях в актуальном
          состоянии и помощи в определении.
        </Card.Body>
        <Card.Body>
          Участники проекта не несут ответственности за точность и достоверность
          предоставленной информации.
        </Card.Body>
      </Card>

      <Footer />
    </Fragment>
  );
}
