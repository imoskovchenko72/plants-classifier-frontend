import { Fragment, ReactElement, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Alert, Button, Container, Spinner } from "react-bootstrap";
import { useMutation } from "react-query";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { PlantDraft } from "../../types/plant";
import { PlantInfoEditable } from "../../components/plant-info-editable/plant-info-editable";
import { createPlant, enrichPlantData } from "../../services/api-actions";
import { AppRoute } from "../../app/app-types";

export function CreatePlantPage(): ReactElement {
  const [plantInfo, setPlantInfo] = useState<PlantDraft>({
    name: "",
    description: "",
    isEndangered: false,
  });

  const createPlantMutation = useMutation(createPlant);
  const enrichDataMutation = useMutation(enrichPlantData, {
    onSuccess: (response) => {
      setPlantInfo(response.data);
    },
  });
  const navigate = useNavigate();

  if (createPlantMutation.isSuccess) {
    navigate(AppRoute.Plant(`${createPlantMutation.data.data.id}`));
  }

  return (
    <Fragment>
      <Header />
      {createPlantMutation.isLoading ? (
        <Spinner />
      ) : createPlantMutation.isError ? (
        <Alert variant="danger">Что-то пошло не так</Alert>
      ) : (
        <Container>
          {enrichDataMutation.isLoading ? (
            <Spinner />
          ) : (
            <PlantInfoEditable
              plantInfo={plantInfo}
              setPlantInfo={setPlantInfo}
            />
          )}
          <div className="control-buttons">
            <Button
              variant="secondary"
              onClick={(e) => enrichDataMutation.mutate(plantInfo)}
            >
              Дополнить информацию
            </Button>
            <Button
              variant="secondary"
              onClick={(e) => createPlantMutation.mutate(plantInfo)}
            >
              Создать
            </Button>
          </div>
        </Container>
      )}
      <Footer />
    </Fragment>
  );
}
