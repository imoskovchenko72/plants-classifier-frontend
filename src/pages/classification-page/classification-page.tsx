import React, { Fragment, ReactElement, useState } from "react";
import { useMutation } from "react-query";
import { Accordion, Button, Container, Spinner, Stack } from "react-bootstrap";

import { Header } from "../../components/header/header";
import { Footer } from "../../components/footer/footer";
import { NumberParam, useQueryParam } from "use-query-params";
import { PlantCharacteristics } from "../../types/plant";
import { PlantCharacteristicsEditable } from "../../components/plant-characteristics-editable/plant-characteristics-editable";
import {
  getMostRelevantPlants,
  getMostRelevantTaxa,
} from "../../services/api-actions";
import { PlantCardsPanel } from "../../components/plant-cards-panel/plant-cards-panel";
import { TaxonCardsPanel } from "../../components/taxon-cards-panel/taxon-cards-panel";
import { SimpleCoordinate } from "../../types/coordinate";

export function ClassificationPage(): ReactElement {
  const [taxonId] = useQueryParam("taxonId", NumberParam);

  const [plantCharacteristics, setPlantCharacteristics] =
    useState<PlantCharacteristics>({});

  const classifyPlantsMutation = useMutation(getMostRelevantPlants);
  const classifyTaxaMutation = useMutation(getMostRelevantTaxa);

  const [coordinates, setCoordinates] = useState<SimpleCoordinate | undefined>(
    undefined,
  );

  return (
    <Fragment>
      <Header />
      <Container>
        <PlantCharacteristicsEditable
          plantCharacteristics={plantCharacteristics}
          setPlantCharacteristics={setPlantCharacteristics}
        />
        <Button
          variant="secondary"
          onClick={(event) => {
            event.preventDefault();
            setCoordinates(
              coordinates === undefined
                ? { latitude: 0, longitude: 0 }
                : undefined,
            );
          }}
        >
          {coordinates === undefined
            ? "Добавить координаты"
            : "Удалить координаты"}
        </Button>
        {coordinates !== undefined && (
          <Stack direction="horizontal" className="items-list" gap={3}>
            Широта:
            <input
              className="table-row-input"
              value={coordinates.latitude}
              type="number"
              onChange={(e) =>
                setCoordinates({
                  ...coordinates,
                  latitude: Number(e.target.value),
                })
              }
            />
            Долгота:
            <input
              className="table-row-input"
              value={coordinates.longitude}
              type="number"
              onChange={(e) =>
                setCoordinates({
                  ...coordinates,
                  longitude: Number(e.target.value),
                })
              }
            />
          </Stack>
        )}

        <div className="control-buttons">
          <Button
            variant="secondary"
            onClick={(event) => {
              event.preventDefault();
              classifyPlantsMutation.mutate({
                request: {
                  taxonId: taxonId === null ? undefined : taxonId,
                  limit: Number(process.env.REACT_APP_RESPONSE_ITEMS_LIMIT),
                },
                characteristics: { ...plantCharacteristics, ...coordinates },
              });
              if (taxonId === undefined) {
                classifyTaxaMutation.mutate({
                  request: {
                    limit: Number(process.env.REACT_APP_RESPONSE_ITEMS_LIMIT),
                  },
                  characteristics: { ...plantCharacteristics, ...coordinates },
                });
              }
            }}
          >
            Определить
          </Button>
        </div>

        {classifyPlantsMutation.isLoading || classifyTaxaMutation.isLoading ? (
          <Spinner />
        ) : (
          <Accordion alwaysOpen className="mb-3">
            {taxonId === undefined &&
              classifyTaxaMutation.data !== undefined && (
                <Accordion.Item eventKey="taxa">
                  <Accordion.Header>
                    Возможные таксоны высшего порядка
                  </Accordion.Header>
                  <Accordion.Body>
                    <TaxonCardsPanel taxonCards={classifyTaxaMutation.data} />
                  </Accordion.Body>
                </Accordion.Item>
              )}
            {classifyPlantsMutation.data !== undefined && (
              <Accordion.Item eventKey="plants">
                <Accordion.Header>Возможные растения</Accordion.Header>
                <Accordion.Body>
                  <PlantCardsPanel plantCards={classifyPlantsMutation.data} />
                </Accordion.Body>
              </Accordion.Item>
            )}
          </Accordion>
        )}
      </Container>

      <Footer />
    </Fragment>
  );
}
