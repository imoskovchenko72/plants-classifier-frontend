import { Fragment, ReactElement, useState } from "react";
import { useQuery } from "react-query";
import { getPlantsByTaxon } from "../../services/api-actions";
import { Button, Container, Spinner } from "react-bootstrap";
import { PlantCardsPanel } from "../../components/plant-cards-panel/plant-cards-panel";
import { Header } from "../../components/header/header";
import { Footer } from "../../components/footer/footer";

export function CatalogPage(): ReactElement {
  const [cardsCount, setCardsCount] = useState<number>(0);

  const { isLoading: isCardsLoading, data: cardsData } = useQuery(
    [`allPlants-${cardsCount}`],
    () => getPlantsByTaxon({ limit: 10 + cardsCount }),
  );

  return (
    <Fragment>
      <Header />
      {isCardsLoading ? (
        <Container
          style={{
            padding: "20% 50%",
          }}
        >
          <Spinner />
        </Container>
      ) : (
        <Container className="plant-card-container">
          <PlantCardsPanel plantCards={cardsData} />
          <Button
            variant="secondary"
            onClick={(event) => {
              event.preventDefault();
              setCardsCount(cardsCount + 10);
            }}
          >
            Загрузить еще
          </Button>
        </Container>
      )}
      <Footer />
    </Fragment>
  );
}
