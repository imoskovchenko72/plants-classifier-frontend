import { Fragment, ReactElement, useState } from "react";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { TaxonInfoEditable } from "../../components/taxon-info-editable/taxon-info-editable";
import { FullTaxonInfo, TaxonType } from "../../types/taxon";
import { useMutation } from "react-query";
import { createTaxon } from "../../services/api-actions";
import { Alert, Button, Container, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { AppRoute } from "../../app/app-types";

export function CreateTaxonPage(): ReactElement {
  const [taxonInfo, setTaxonInfo] = useState<FullTaxonInfo>({
    name: "",
    description: "",
    type: TaxonType.DIVISION,
  });

  const createTaxonMutation = useMutation(createTaxon);

  const navigate = useNavigate();

  if (createTaxonMutation.isSuccess) {
    navigate(AppRoute.Taxon(`${createTaxonMutation.data.data.id}`));
  }

  return (
    <Fragment>
      <Header />
      {createTaxonMutation.isLoading ? (
        <Spinner />
      ) : createTaxonMutation.isError ? (
        <Alert variant="danger">Что-то пошло не так</Alert>
      ) : (
        <Container>
          <TaxonInfoEditable
            taxonInfo={taxonInfo}
            setTaxonInfo={setTaxonInfo}
            isSetTypeForbidden={false}
          />
          <div className="control-buttons">
            <Button
              variant="secondary"
              onClick={(e) => createTaxonMutation.mutate(taxonInfo)}
            >
              Создать
            </Button>
          </div>
        </Container>
      )}

      <Footer />
    </Fragment>
  );
}
