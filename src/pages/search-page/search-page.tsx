import { Fragment, ReactElement } from "react";

import { Footer } from "../../components/footer/footer";
import { Header } from "../../components/header/header";
import { NumberParam, StringParam, useQueryParam } from "use-query-params";
import { Accordion, Spinner } from "react-bootstrap";
import { useQuery } from "react-query";
import { searchPlants, searchTaxa } from "../../services/api-actions";
import { PlantCardsPanel } from "../../components/plant-cards-panel/plant-cards-panel";
import { TaxonCardsPanel } from "../../components/taxon-cards-panel/taxon-cards-panel";

export function SearchPage(): ReactElement {
  const [query] = useQueryParam("query", StringParam);
  const [taxonId] = useQueryParam("taxonId", NumberParam);

  const { isLoading: isPlantCardsLoading, data: plantCardsData } = useQuery(
    [`searchPlants-${query}`],
    () =>
      searchPlants({
        taxonId: taxonId === null ? undefined : taxonId,
        query: query === null ? undefined : query,
        limit: Number(process.env.REACT_APP_RESPONSE_ITEMS_LIMIT),
      }),
  );

  const { isLoading: isTaxonCardsLoading, data: taxonCardsData } = useQuery(
    [`searchTaxa-${query}`],
    () =>
      searchTaxa({
        query: query === null ? undefined : query,
        limit: Number(process.env.REACT_APP_RESPONSE_ITEMS_LIMIT),
      }),
  );

  return (
    <Fragment>
      <Header />
      <Accordion defaultActiveKey={["taxa", "plants"]} alwaysOpen>
        <h1>{query}</h1>
        {!taxonId && (
          <Accordion.Item eventKey="taxa">
            <Accordion.Header>Таксоны</Accordion.Header>
            <Accordion.Body>
              {isTaxonCardsLoading ? (
                <Spinner />
              ) : (
                <TaxonCardsPanel taxonCards={taxonCardsData} />
              )}
            </Accordion.Body>
          </Accordion.Item>
        )}
        <Accordion.Item eventKey="plants">
          <Accordion.Header>Растения</Accordion.Header>
          <Accordion.Body>
            {isPlantCardsLoading ? (
              <Spinner />
            ) : (
              <PlantCardsPanel plantCards={plantCardsData} />
            )}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
      <Footer />
    </Fragment>
  );
}
