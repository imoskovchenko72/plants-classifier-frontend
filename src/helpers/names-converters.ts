export function makeFirstLetterUpper(s: string): string {
  return s.slice(0, 1).toUpperCase() + s.slice(1);
}
