import { FullPlantInfo, PlantDraft } from "../types/plant";

export function convertFullPlantInfoToPlantDraft(
  plantInfo: FullPlantInfo,
): PlantDraft {
  return {
    ...plantInfo,
    division: plantInfo.division?.name,
    plantClass: plantInfo.plantClass?.name,
    ordo: plantInfo.ordo?.name,
    familia: plantInfo.familia?.name,
    genus: plantInfo.genus?.name,
    species: plantInfo.species?.name,
  };
}
