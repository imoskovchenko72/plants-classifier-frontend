import { PlantCharacteristicsType } from "../types/plantCharacteristics";

export function characteristicsTypeToRussianName(
  characteristics: PlantCharacteristicsType,
) {
  if (characteristics === PlantCharacteristicsType.LIFE_FORM) {
    return "Жизненная форма";
  } else if (characteristics === PlantCharacteristicsType.SHOOTS_TYPE) {
    return "Побеги";
  } else if (characteristics === PlantCharacteristicsType.LEAF_ARRANGEMENT) {
    return "Листорасположение";
  } else if (characteristics === PlantCharacteristicsType.LEAF_PLACEMENT) {
    return "Размещение листьев";
  } else if (characteristics === PlantCharacteristicsType.SHOOTS_SURFACE) {
    return "Поверхность побегов";
  } else if (characteristics === PlantCharacteristicsType.APPENDAGES) {
    return "Придатки";
  } else if (characteristics === PlantCharacteristicsType.LEAF_TYPE) {
    return "Тип листьев";
  } else if (characteristics === PlantCharacteristicsType.LEAF_BLADE_SHAPE) {
    return "Форма листовой пластинки";
  } else if (characteristics === PlantCharacteristicsType.LEAF_BLADE_DIVISION) {
    return "Членение листовой пластинки";
  } else if (
    characteristics === PlantCharacteristicsType.LEAF_BLADE_COMPLEXITY_LEVEL
  ) {
    return "Порядок сложности листовой пластинки";
  } else if (
    characteristics === PlantCharacteristicsType.LEAF_BLADE_ATTACHMENT
  ) {
    return "Крепление листовой пластинки";
  } else if (characteristics === PlantCharacteristicsType.LEAF_PARTS_SHAPE) {
    return "Форма долей листьев";
  } else if (characteristics === PlantCharacteristicsType.LEAF_PARTS_DIVISION) {
    return "Членение долей листьев";
  } else if (characteristics === PlantCharacteristicsType.LEAF_APEX) {
    return "Верхушка листа";
  } else if (characteristics === PlantCharacteristicsType.LEAF_MARGIN) {
    return "Край листа";
  } else if (characteristics === PlantCharacteristicsType.LEAF_BASE) {
    return "Основание листа";
  } else if (characteristics === PlantCharacteristicsType.LEAF_SURFACE) {
    return "Поверхность листа";
  } else if (characteristics === PlantCharacteristicsType.LEAF_APPENDAGES) {
    return "Придатки листа";
  } else if (characteristics === PlantCharacteristicsType.INFLORESCENSE) {
    return "Соцветие";
  } else if (characteristics === PlantCharacteristicsType.FLOWER_MAIN_COLOUR) {
    return "Основной цвет цветка";
  } else if (characteristics === PlantCharacteristicsType.FLOWER_COLOUR_TINTS) {
    return "Оттенки цветка";
  } else if (
    characteristics === PlantCharacteristicsType.FLOWER_SPOTS_AND_STRIPES
  ) {
    return "Пятна и полоски цветка";
  } else if (characteristics === PlantCharacteristicsType.FLOWER_SIZE) {
    return "Размер цветка";
  } else if (characteristics === PlantCharacteristicsType.PERIANTH) {
    return "Околоцветник";
  } else if (characteristics === PlantCharacteristicsType.PETALS_NUMBER) {
    return "Число лепестков";
  } else if (characteristics === PlantCharacteristicsType.FRUITS_TYPE) {
    return "Тип плодов";
  } else if (characteristics === PlantCharacteristicsType.FRUIT_COLOUR) {
    return "Окраска плодов";
  } else if (characteristics === PlantCharacteristicsType.FRUIT_APPENDAGES) {
    return "Придатки плодов";
  } else if (characteristics === PlantCharacteristicsType.CONES_CONSISTENCY) {
    return "Консистенция шишек";
  } else if (characteristics === PlantCharacteristicsType.CONES_SHAPE) {
    return "Форма шишек";
  } else if (characteristics === PlantCharacteristicsType.SPORANGIA) {
    return "Спорангии";
  } else {
    // It never happens
    return "Неизвестный признак";
  }
}
