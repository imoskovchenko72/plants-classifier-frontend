import { ApiRoute, BACKEND_URL } from "../services/api-constants";

export function getFullFormatPhotoUrl(photoId: string): string {
  return `${BACKEND_URL}${ApiRoute.GetFullFormatPhoto(photoId)}`;
}

export function getMiniaturePhotoUrl(photoId: string): string {
  return `${BACKEND_URL}${ApiRoute.GetMiniaturePhoto(photoId)}`;
}
