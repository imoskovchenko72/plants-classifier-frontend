import { TaxonType } from "../types/taxon";

export function taxonTypeToRussianName(taxon: TaxonType) {
  if (taxon === TaxonType.DIVISION) {
    return "Отдел";
  } else if (taxon === TaxonType.CLASS) {
    return "Класс";
  } else if (taxon === TaxonType.ORDO) {
    return "Порядок";
  } else if (taxon === TaxonType.FAMILIA) {
    return "Семейство";
  } else if (taxon === TaxonType.GENUS) {
    return "Род";
  } else if (taxon === TaxonType.SPECIES) {
    return "Вид";
  } else {
    // It never happens
    return "Неизвестный науке зверь";
  }
}
