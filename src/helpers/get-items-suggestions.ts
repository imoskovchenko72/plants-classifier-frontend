export function getItemsSuggestions(
  alreadyUsedItems: string[],
  query: string,
  possibleItems?: string[],
): string[] {
  query = query.toLowerCase();
  if (possibleItems === undefined) {
    if (query) {
      return [query];
    }
    return [];
  }
  const filteredPossibleItems = possibleItems
    .map((item) => item.toLowerCase())
    .filter(
      (value) =>
        value !== query &&
        value.startsWith(query) &&
        !alreadyUsedItems.includes(value),
    );
  if (query) {
    return [query].concat(filteredPossibleItems.slice(0, 9));
  }
  return filteredPossibleItems.slice(0, 9);
}
