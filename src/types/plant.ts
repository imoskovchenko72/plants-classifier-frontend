import { ShortTaxonInfo } from "./taxon";

export type ShortPlantInfo = {
  id: number;
  name: string;
  isEndangered: boolean;
  russianName?: string;
  photoId?: string;
};

export type PlantCharacteristics = {
  lifeForm?: string[];
  shootsType?: string[];
  leafArrangement?: string[];
  leafPlacement?: string[];
  shootsSurface?: string[];
  appendages?: string[];
  leafType?: string[];
  leafBladeShape?: string[];
  leafBladeDivision?: string[];
  leafBladeComplexityLevel?: string[];
  leafBladeAttachment?: string[];
  leafPartsShape?: string[];
  leafPartsDivision?: string[];
  leafApex?: string[];
  leafMargin?: string[];
  leafBase?: string[];
  leafSurface?: string[];
  leafAppendages?: string[];
  inflorescense?: string[];
  flowerMainColour?: string[];
  flowerColourTints?: string[];
  flowerSpotsAndStripes?: string[];
  flowerSize?: string[];
  perianth?: string[];
  petalsNumber?: string[];
  fruitsType?: string[];
  fruitColour?: string[];
  fruitAppendages?: string[];
  conesConsistency?: string[];
  conesShape?: string[];
  sporangia?: string[];
};

export type PlantTaxonomy = {
  division?: ShortTaxonInfo;
  plantClass?: ShortTaxonInfo;
  ordo?: ShortTaxonInfo;
  familia?: ShortTaxonInfo;
  genus?: ShortTaxonInfo;
  species?: ShortTaxonInfo;
};

export type NotCreatedYetPlantTaxonomy = {
  division?: string;
  plantClass?: string;
  ordo?: string;
  familia?: string;
  genus?: string;
  species?: string;
};

export type PlantNamesAndDescription = {
  name: string;
  isEndangered: boolean;
  russianNames?: string[];
  description?: string;
};

export type PlantDraft = PlantCharacteristics &
  NotCreatedYetPlantTaxonomy &
  PlantNamesAndDescription;

export type FullPlantInfo = PlantCharacteristics &
  PlantTaxonomy &
  PlantNamesAndDescription & {
    id: number;
  };
