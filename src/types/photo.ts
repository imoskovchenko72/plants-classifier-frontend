export type ShortPhotoInfo = {
  description?: string;
  plantId: number;
};

export type Photo = {
  id: string;
  description?: string;
  filename?: string;
  plantId: number;
};
