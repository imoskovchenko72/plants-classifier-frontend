export enum PlantCharacteristicsType {
  LIFE_FORM = "life_form",
  //shoots
  SHOOTS_TYPE = "shoots_type",
  LEAF_ARRANGEMENT = "leaf_arrangement",
  LEAF_PLACEMENT = "leaf_placement",
  SHOOTS_SURFACE = "shoots_surface",
  APPENDAGES = "appendages",
  //leaves
  LEAF_TYPE = "leaf_type",
  LEAF_BLADE_SHAPE = "leaf_blade_shape",
  LEAF_BLADE_DIVISION = "leaf_blade_division",
  LEAF_BLADE_COMPLEXITY_LEVEL = "leaf_blade_complexity_level",
  LEAF_BLADE_ATTACHMENT = "leaf_blade_attachment",
  LEAF_PARTS_SHAPE = "leaf_parts_shape",
  LEAF_PARTS_DIVISION = "leaf_parts_division",
  LEAF_APEX = "leaf_apex",
  LEAF_MARGIN = "leaf_margin",
  LEAF_BASE = "leaf_base",
  LEAF_SURFACE = "leaf_surface",
  LEAF_APPENDAGES = "leaf_appendages",
  //flowers
  INFLORESCENSE = "inflorescense",
  FLOWER_MAIN_COLOUR = "flower_main_colour",
  FLOWER_COLOUR_TINTS = "flower_colour_tints",
  FLOWER_SPOTS_AND_STRIPES = "flower_spots_and_stripes",
  FLOWER_SIZE = "flower_size",
  PERIANTH = "perianth",
  PETALS_NUMBER = "petals_number",
  //fruits
  FRUITS_TYPE = "fruits_type",
  FRUIT_COLOUR = "fruit_colour",
  FRUIT_APPENDAGES = "fruit_appendages",
  //cones
  CONES_CONSISTENCY = "cones_consistency",
  CONES_SHAPE = "cones_shape",
  //sporangia
  SPORANGIA = "sporangia",
}
