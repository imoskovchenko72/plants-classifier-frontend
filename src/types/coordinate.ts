export type SimpleCoordinate = {
  latitude: number;
  longitude: number;
};

export type CoordinateInfo = SimpleCoordinate & {
  longitude: number;
  plantId: number;
  description?: string;
  date?: string;
};

export type CoordinateWithId = CoordinateInfo & {
  id: number;
};
