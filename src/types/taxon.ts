export enum TaxonType {
  DIVISION = "division",
  CLASS = "plant_class",
  ORDO = "ordo",
  FAMILIA = "familia",
  GENUS = "genus",
  SPECIES = "species",
}

export type ShortTaxonInfo = {
  id: number;
  name: string;
};

export type FullTaxonInfo = {
  name: string;
  type: TaxonType;
  description?: string;
  russianNames?: string[];
};

export type FullTaxonInfoWithId = FullTaxonInfo & {
  id: number;
};
