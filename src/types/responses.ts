export type WithRelevanceResponse = {
  relevanceRatio?: number;
};
