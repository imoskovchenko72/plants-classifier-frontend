export type WithLimitRequest = {
  limit?: number;
  offset?: number;
};

export type WithTaxonomyRequest = {
  taxonId?: number;
};

export type WithQueryRequest = {
  query?: string;
};
