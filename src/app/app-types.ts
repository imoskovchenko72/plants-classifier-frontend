export const AppRoute = {
  Root: "/",
  Search: "/search",
  Catalog: "/catalog",
  Classification: "/classification",
  Plant: (plantId?: string) => `/plant/${plantId || ":id"}`,
  Taxon: (taxonId?: string) => `/taxon/${taxonId || ":id"}`,
  CreatePlant: "/plant/create",
  CreateTaxon: "/taxon/create",
  EditPlant: (plantId?: string) => `/plant/${plantId || ":id"}/edit`,
  EditTaxon: (taxonId?: string) => `/taxon/${taxonId || ":id"}/edit`,
};
