import { ReactElement } from "react";
import { HelmetProvider } from "react-helmet-async";
import { Route, Routes, BrowserRouter } from "react-router-dom";

import { AppRoute } from "./app-types";
import { MainPage } from "../pages/main-page/main-page";
import { PlantPage } from "../pages/plant-page/plant-page";
import { TaxonPage } from "../pages/taxon-page/taxon-page";
import { NotFoundPage } from "../pages/not-found-page/not-found-page";
import { SearchPage } from "../pages/search-page/search-page";
import { QueryParamProvider } from "use-query-params";
import { ReactRouter6Adapter } from "use-query-params/adapters/react-router-6";
import { CatalogPage } from "../pages/catalog-page/catalog-page";
import { ClassificationPage } from "../pages/classification-page/classification-page";
import { CreateTaxonPage } from "../pages/create-taxon-page/create-taxon-page";
import { CreatePlantPage } from "../pages/create-plant-page/create-plant-page";
import { EditPlantPage } from "../pages/edit-plant-page/edit-plant-page";
import { EditTaxonPage } from "../pages/edit-taxon-page/edit-taxon-page";

export function App(): ReactElement {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <QueryParamProvider adapter={ReactRouter6Adapter}>
          <Routes>
            <Route path={AppRoute.Root} element={<MainPage />} />
            <Route path={AppRoute.Search} element={<SearchPage />} />
            <Route
              path={AppRoute.Classification}
              element={<ClassificationPage />}
            />
            <Route path={AppRoute.Catalog} element={<CatalogPage />} />
            <Route path={AppRoute.Plant()} element={<PlantPage />} />
            <Route path={AppRoute.Taxon()} element={<TaxonPage />} />
            <Route path={AppRoute.CreatePlant} element={<CreatePlantPage />} />
            <Route path={AppRoute.CreateTaxon} element={<CreateTaxonPage />} />
            <Route path={AppRoute.EditPlant()} element={<EditPlantPage />} />
            <Route path={AppRoute.EditTaxon()} element={<EditTaxonPage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </QueryParamProvider>
      </BrowserRouter>
    </HelmetProvider>
  );
}
